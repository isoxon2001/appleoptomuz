package com.example.smarlife.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.smarlife.modellar.CategoryModel
import com.example.smarlife.modellar.Top_Product_Model

@Database(entities = [CategoryModel::class,Top_Product_Model::class], version = 1)
abstract class AppDataBase:RoomDatabase(){
    abstract fun getCategoryDao():CategoryDao
    abstract fun getTopProductDao():TopProductDao
    companion object{
        // singlton pattern
        var INSTACE:AppDataBase? =null
        fun initDatabase(context: Context){
            if (INSTACE==null){
                synchronized(AppDataBase::class){
                    INSTACE= Room.databaseBuilder(context.applicationContext,AppDataBase::class.java,"online_shop1").build()
                }
            }
        }
        fun getDatabase():AppDataBase{
            return INSTACE!!
        }
    }
}