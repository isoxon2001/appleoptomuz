package com.example.smarlife.Splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.example.smarlife.Login.LoginOyna
import com.example.smarlife.Login.RuhatdanUtish.RegistrActivity
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.main.MainActivity
import com.example.smarlife.utils.PrefUtils
import com.google.firebase.auth.FirebaseAuth


class SplashActivity : AppCompatActivity() {

    val TAG = "SplashActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        var handler = Handler(Looper.getMainLooper())
        handler.postDelayed({


            Log.d(TAG, "onCreate: ${FirebaseAuth.getInstance().currentUser?.phoneNumber}")
            Log.d(TAG, "onCreate: ${PrefUtils.getToken()}")
            if (FirebaseAuth.getInstance().currentUser?.phoneNumber == null) {
                var intent = Intent(this, LoginOyna::class.java)
                startActivity(intent)
                finish()
            } else if (PrefUtils.getToken().isEmpty()) {
                var intent = Intent(this, RegistrActivity::class.java)
                intent.putExtra("phoneNumber", FirebaseAuth.getInstance().currentUser?.phoneNumber)
                startActivity(intent)
                finish()
            } else {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, 3000)
    }
}