package com.example.smarlife.fragmentlar.main

import android.icu.text.Transliterator
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

class CyrillicToLatin {
        companion object{


            private val rules =  /*"::[А-ЪЬЮ-ъьюяѢѣѪѫ];" +*/"а > a;" +
                "б > b;" +
                "в > v;" +
                "г > g;" +
                "д > d;" +
                "е > e;" +
                "ё > yo;" +
                "ж > j;" +
                "з > z;" +
                "и > i;" +
                "й > y;" +
                "к > k;" +
                "л > l;" +
                "м > m;" +
                "н > n;" +
                "о > o;" +
                "п > p;" +
                "р > r;" +
                "с > s;" +
                "т > t;" +
                "у > u;" +
                "ф > f;" +
                "х > h;" +
                "ц > w;" +
                "ч > ch;" +
                "ш > sh;" +
                "щ > sh;" +
                "ъ > ;" +
                "ы > i;" +
                "ь > ;" +
                "э > e;" +
                "ю > yu;" +
                "x > h;" +  //            "`'` > ;"+
                "я > ya;"

            @RequiresApi(Build.VERSION_CODES.Q)
            fun transliterate(massage: String): String? {

                val transliterator = Transliterator.createFromRules("temp", rules, Transliterator.FORWARD)
                return transliterator.transliterate(massage.toLowerCase(Locale.ROOT))

            }

    }


}
