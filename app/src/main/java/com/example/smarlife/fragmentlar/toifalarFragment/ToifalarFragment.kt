package com.example.smarlife.fragmentlar.toifalarFragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.modellar.request.PostRegisterRequest
import com.example.smarlife.view.ToifalarAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_savat.*
import kotlinx.android.synthetic.main.fragment_toifalar.*


class ToifalarFragment : Fragment() {
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel= ViewModelProvider(this).get(MainViewModel::class.java)

//        viewModel.progress.observe(this, Observer{
//
//            swipeToifa.isRefreshing=it
//        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_toifalar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // swipe uchun
//        swipeToifa.setOnRefreshListener {
//            loadData()
//
//        }


        resycleToifa.layoutManager= LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL,false)

        // bu toifalardi itemi ezilganda ochilishi uchun
        viewModel.toifa_product_Data.observe(requireActivity(),{
            resycleToifa.adapter= ToifalarAdapter(it)

            Log.e("eee", it.toString())

        })
        // swipe qilganda progress ni yangilaydi
//        viewModel.progress.observe(requireActivity(), Observer {
//            swipeToifa.isRefreshing=it
//        })


//            viewModel.top_product_Data.observe(requireActivity(),{
//                category_ochish.adapter= Top_Product_Adapter(it)
//
//                Log.e("eee", it.toString())
//
//            })

//        viewModel.toifa_product_Data.observe(requireActivity(), Observer {
//            resayclerCategories.adapter= CategoryAdapter(it, object : CategoryAdapterCallBack {
//                // bu categoris ezilganda mahsulotlar chiqishi uchun
//                override fun onClickItem(item: CategoryModel) {
//                    viewModel.getProductsByCategory(item.id)
//
//                }
//            })
//        })
        loadData()

    }
//    // bu Funksiya doim ochilganda yangilaydi
//    override fun onResume() {
//        super.onResume()
//        loadData()
//
//
//    }
    fun loadData(){
        viewModel.getToifa()
        // bu Room uchun
//        viewModel.getAllDBCategorys()

//    // postregistr
//    val postRegisterRequest=PostRegisterRequest("sc","ds","sjdh")
//    viewModel.gettoken(postRegisterRequest)
    }

    companion object {
          @JvmStatic
        fun newInstance() = ToifalarFragment()
    }
}