package com.example.smarlife.fragmentlar.homeFragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.R
import com.example.smarlife.modellar.CategoryModel
import com.example.smarlife.modellar.OfferModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import com.example.smarlife.view.CategoryAdapter
import com.example.smarlife.view.CategoryAdapterCallBack
import com.example.smarlife.view.DiffUtil.TopProductAdapterDiffutil
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {
    // rasimlarni karuselga quyish uchun elon qilish
    var offers: List<OfferModel> = emptyList()

    // AdapterDiffUtil ni elon qilish
    val topProductAdapter = TopProductAdapterDiffutil()


    // MainViewModel bilan boglash LiveData
    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    // karuselni ishga tushirish
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // swipe uchun
//        swipe.setOnRefreshListener {
//            loadData()
//
//        }

        // bu API ga murojat qilish  load Datani chaqirmasa malumotlar kelmaydi
        loadData()

        resayclerCategories.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)

        // bu yerda Live datani malumotlarini homefragmentga obsorfe qilish
        viewModel.error.observe(requireActivity(), Observer {
            Toast.makeText(requireActivity(), it, Toast.LENGTH_LONG).show()
        })
        viewModel.offersData.observe(requireActivity(), Observer {

            carouselView.setImageListener { position, imageView ->

                Glide.with(imageView).load(Constants.HOST_IMAGE + it[position].image)
                    .into(imageView)

            }
            carouselView.pageCount = it.count()

        })
        // call back ni observer qilish
        viewModel.categorysData.observe(requireActivity(), Observer {
            resayclerCategories.adapter = CategoryAdapter(it, object : CategoryAdapterCallBack {
                // bu categoris ezilganda mahsulotlar chiqishi uchun
                override fun onClickItem(item: CategoryModel) {
                    viewModel.getProductsByCategory(item.id)

                }
            })
        })

        // top productlarni olib kelish uchun Viewmodelni kuzatish
        recyclerViewProducts = view.findViewById(R.id.resayclerTopProduct)
        recyclerViewProducts.adapter = topProductAdapter


//        resayclerTopProduct.layoutManager =
//            GridLayoutManager(requireActivity(),2)

        resayclerTopProduct.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)


        // listni elon qilish
        viewModel.top_product_Data.observe(requireActivity(), {
            listProduct = ArrayList()
            listProduct.addAll(it)

            // bu yerda submit qilib quyiladi AdapterDiffUtil ni
            // submit qilish DiffUtilni submit qilib quyiladi
            topProductAdapter.submitList(listProduct)


            Log.e("eee", it.toString())

        })

//    //     swipe qilganda progress ni yangilaydi
//        viewModel.progress.observe(requireActivity(), Observer {
//            swipe.isRefreshing = it
//        })


    }

    // qaytganda bu Funksiya doim ochilganda yangilaydi
    override fun onResume() {
        super.onResume()
        Log.d("asdf","loaddata")
//        loadData()
    }

    //  Load Data Bu MainViewModel dagi Funksiyalarni chaqirib quyamiz
    //  datalar yani Malumotlarni olib kelib beradi
    fun loadData() {
        viewModel.getOffers()
        viewModel.getCategories()
        viewModel.getTopProduct()
//        // bular DB uchun Room uchun
//        viewModel.getAllDBTopProducts()
//        viewModel.getAllDBCategorys()
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()

        lateinit var listProduct: ArrayList<Top_Product_Model>
        lateinit var recyclerViewProducts: RecyclerView
    }


}