package com.example.smarlife.fragmentlar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.databinding.ActivityToifaOchishBinding
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import com.example.smarlife.view.AdapterToifaOchish
import kotlinx.android.synthetic.main.toifa_ochish.*


class ToifaOchishActivity : AppCompatActivity() {
    // MainViewModel bilan boglash LiveData
    lateinit var viewModel: MainViewModel
    lateinit var item: Top_Product_Model

    private lateinit var binding: ActivityToifaOchishBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityToifaOchishBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toifa_ochish_uchun:Int = intent.getIntExtra(Constants.EXTRA_DATA, 0)
       // Bu yerda Extra data orqali icon va title olib kelinyapdi
        val titleCategory = intent.getStringExtra("title")
        val iconCategory = intent.getStringExtra("icon")


       // Toast.makeText(this, toifa_ochish_uchun.toString(), Toast.LENGTH_SHORT).show()


        binding.tvTitleToifa.text = titleCategory
        Glide.with(binding.imgFavoriteToifa).load(iconCategory).into(binding.imgFavoriteToifa)







        binding.cardViewBackToifa.setOnClickListener {
            finish()
        }

        viewModel = MainViewModel()
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getProductsByCategory(toifa_ochish_uchun)

        viewModel.top_product_Data.observe(this, Observer {
            Log.e("prs",it.toString())
            binding.categoryOchishRv.adapter = AdapterToifaOchish(it)
        })

    }


}