package com.example.smarlife.fragmentlar.main

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.InfoSLActivity
import com.example.smarlife.fragmentlar.favoritFragment.FavoriteFragment
import com.example.smarlife.fragmentlar.homeFragment.HomeFragment
import com.example.smarlife.fragmentlar.profileFragment.ProfileFragment
import com.example.smarlife.fragmentlar.savatFragment.SavatFragment
import com.example.smarlife.fragmentlar.toifalarFragment.ToifalarFragment
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.LocalManger
import com.example.smarlife.utils.NetworkConnection
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {




    var list = ArrayList<Top_Product_Model>()
    var klients = ArrayList<Top_Product_Model>()

    //    val kriltolatin= CyrillicToLatin()
    // bu fragmentlarni elon qilish
    val homeFragment = HomeFragment.newInstance()
    val toifalarFragment = ToifalarFragment.newInstance()
    val savatFragment = SavatFragment.newInstance()
    val profilFragment = ProfileFragment.newInstance()
    val favoriteFragment = FavoriteFragment.newInstance()
    var activeFragment: Fragment = homeFragment

    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        viewModel = MainViewModel()



        // internet yonganda olib kelish
        val networkConnection = NetworkConnection(this)
        networkConnection.observe(this, androidx.lifecycle.Observer { isConnected ->
            if (isConnected) {
                homeFragment.loadData()
                toifalarFragment.loadData()
                savatFragment.loadData()
                favoriteFragment.loadData()
            } else {
                Toast.makeText(this, getString(R.string.internet_yoq), Toast.LENGTH_SHORT).show()
            }

        })

//        //DB uchun insert
//        viewModel.top_product_Data.observe(this, androidx.lifecycle.Observer {
//            viewModel.insertAllProducts2DB(it)
//            homeFragment.loadData()
//        })
//
//
//        //DB uchun insert Categorys uchun
//        viewModel.categorysData.observe(this, androidx.lifecycle.Observer {
//            viewModel.insertAllCategorys2DB(it)
//            homeFragment.loadData()
//        })


        // Toast kurinishi uchun
        viewModel.error.observe(this, androidx.lifecycle.Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })


        // intent uchun
        logoSmartLife.setOnClickListener {

            val intent = Intent(this, InfoSLActivity::class.java)
            this.startActivity(intent)
        }
// Bu Button Navigationlarni Prazrachi Qilib beradi
//        button_nav_bar.background=null


        supportFragmentManager.beginTransaction()
            .add(R.id.flContainer, homeFragment, homeFragment.tag).hide(homeFragment)
            .commit()
        supportFragmentManager.beginTransaction()
            .add(R.id.flContainer, toifalarFragment, toifalarFragment.tag).hide(toifalarFragment)
            .commit()
        supportFragmentManager.beginTransaction()
            .add(R.id.flContainer, savatFragment, savatFragment.tag).hide(savatFragment).commit()
        supportFragmentManager.beginTransaction()
            .add(R.id.flContainer, favoriteFragment, favoriteFragment.tag).hide(favoriteFragment)
            .commit()

        supportFragmentManager.beginTransaction()
            .add(R.id.flContainer, profilFragment, profilFragment.tag).hide(profilFragment).commit()

        // bu birinchi holatda activeFragmentni ishga tushiradi
        supportFragmentManager.beginTransaction().show(activeFragment).commit()


        // buttonlar ezilganda ochilishi uchun
        button_nav_bar.setOnItemSelectedListener() { id ->
            if (id == R.id.nav_home) {
                supportFragmentManager.beginTransaction().hide(activeFragment).show(homeFragment)
                    .commit()
                activeFragment = homeFragment

            } else if (id == R.id.nav_toifalar) {
                supportFragmentManager.beginTransaction().hide(activeFragment)
                    .show(toifalarFragment).commit()
                activeFragment = toifalarFragment
            } else if (id == R.id.nav_savat) {
                supportFragmentManager.beginTransaction().hide(activeFragment).show(savatFragment)
                    .commit()
                activeFragment = savatFragment
            } else if (id == R.id.nav_fovorite) {
                supportFragmentManager.beginTransaction().hide(activeFragment)
                    .show(favoriteFragment).commit()
                activeFragment = favoriteFragment
            } else if (id == R.id.nav_profil) {
                supportFragmentManager.beginTransaction().hide(activeFragment).show(profilFragment)
                    .commit()
                activeFragment = profilFragment
            }
            true


        }


        // search uchun


        qidiruv.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            @RequiresApi(Build.VERSION_CODES.Q)
            override fun onTextChanged(newText: CharSequence?, p1: Int, p2: Int, p3: Int) {


                var newitems = ArrayList<Top_Product_Model>()
                newitems.clear()


                var text = newText.toString()


                if (text.isEmpty() || newText == "") {
                    newitems.addAll(list)

                } else {
                    val spl_keys: Array<String> =
                        CyrillicToLatin.transliterate(newText.toString().trim())
                            ?.split(" ".toRegex())!!
                            .toTypedArray()
                    var lowercase = CyrillicToLatin.transliterate(text.toLowerCase())

                    for (d in list) {
                        val name = CyrillicToLatin.transliterate(d.name_uz)
                        var ok = true
                        for (x in spl_keys)
                            if (name?.indexOf(x) == -1) {
                                ok = false
                                break
                            }
                        if (ok) {
                            newitems.add(d)
                        }
                    }

                }
                HomeFragment.listProduct.clear()
                HomeFragment.listProduct.addAll(newitems)
                HomeFragment.recyclerViewProducts.adapter!!.notifyDataSetChanged()


            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })


        loadData()

    }


    fun loadData() {
        viewModel.getTopProduct()
        viewModel.getCategories()
   //     viewModel.getOffers()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocalManger.setLocale(newBase))
    }


}