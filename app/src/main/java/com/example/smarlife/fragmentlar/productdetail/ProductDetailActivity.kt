package com.example.smarlife.fragmentlar.productdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppCompatActivity() {
    lateinit var item:Top_Product_Model
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

            cardViewBack.setOnClickListener {
                finish()
            }




        cardViewFovorid.setOnClickListener {
            PrefUtils.setFavorite(item)
            if (PrefUtils.checkFavorite(item)){
                imgFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
            }else{
                imgFavorite.setImageResource(R.drawable.ic_heart)
            }
        }




        // bu malumotlarni uzatish uchun
        item=intent.getSerializableExtra(Constants.EXTRA_DATA)as Top_Product_Model



        tvTitle.text=item.name_uz
        tvProductName.text=item.name_uz
        tvPrice.text=item.price_usd+" $"
        tvProductKoment.text=item.description_uz

        // bu agar mahsulot savatda bulsa btnAdd2Card button kurinmaydi
        if (PrefUtils.getCartCount(item)>0){
            btnAdd2Card.visibility=View.GONE
        }


            if (PrefUtils.checkFavorite(item)){
                imgFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
            }else{
                imgFavorite.setImageResource(R.drawable.ic_heart)
            }

        Glide.with(this).load(Constants.HOST_IMAGE+item.image).into(imgProduct)

        // bu btnAdd2Card bosilganda mahsulotlarni 1-dona qilib savatga qushadi
        btnAdd2Card.setOnClickListener{
            item.cardCount =1
            PrefUtils.setCard(item)
            Toast.makeText(this, "Mahsulot Savatga Qo`shildi", Toast.LENGTH_SHORT).show()
            finish()
        }


//        toifa_cardViewFovorid.setOnClickListener {
//            PrefUtils.setFavorite(item)
//            if (PrefUtils.checkFavorite(item)){
//                toifa_imgFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
//            }else{
//                toifa_imgFavorite.setImageResource(R.drawable.ic_heart)
//            }
//        }
//
//        if (PrefUtils.checkFavorite(item)){
//            toifa_imgFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
//        }else{
//            toifa_imgFavorite.setImageResource(R.drawable.ic_heart)
//        }
    }
}