package com.example.smarlife.fragmentlar.favoritFragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.homeFragment.HomeFragment
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.fragmentlar.profileFragment.ProfileFragment
import com.example.smarlife.utils.PrefUtils
import com.example.smarlife.view.DiffUtil.TopProductAdapterDiffutil
import com.example.smarlife.view.Top_Product_Adapter
import kotlinx.android.synthetic.main.fragment_favorite.*

class FavoriteFragment : Fragment() {
    // bu viewModel bilan boglash
    lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel=ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.top_product_Data.observe(this, Observer {

            resayclerProductByIds.adapter=Top_Product_Adapter(it)

        })
        // bu observer qilish
        viewModel.progress.observe(this, Observer {
            swipe.isRefreshing=it
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
            // Fragmentda binding uchun
//        binding= FragmentRegistrBinding.inflate(inflater,container,false)
//        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resayclerProductByIds.layoutManager=LinearLayoutManager(requireActivity())


//        viewModel=ViewModelProvider(this).get(MainViewModel::class.java)
//
//        viewModel.top_product_Data.observe(lifecycle, Observer {
//
//            resayclerProductByIds.adapter=Top_Product_Adapter(it)
//
//        })
//        // bu observer qilish
//        viewModel.progress.observe(lifecycle, Observer {
//            swipe.isRefreshing=it
//        })



        swipe.setOnRefreshListener {
            loadData()
        }
        loadData()
    }

    // qaytganda load data qiladi
    override fun onResume() {
        super.onResume()
//        loadData()


    }
    fun loadData(){
        var string =""
        PrefUtils.getFavoriteList().forEach {
            string=string+it.toString()+","

        }
       // Log.e("asd",string.substringBeforeLast(','))
        viewModel.getProductsByIds(string.substringBeforeLast(","))

    }

    companion object {
        @JvmStatic
        fun newInstance() = FavoriteFragment()
    }
}