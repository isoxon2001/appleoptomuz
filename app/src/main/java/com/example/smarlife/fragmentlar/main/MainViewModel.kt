package com.example.smarlife.fragmentlar.main

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smarlife.api.repository.ShopRepository
import com.example.smarlife.db.AppDataBase
import com.example.smarlife.modellar.CategoryModel
import com.example.smarlife.modellar.OfferModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.modellar.UserModel
import com.example.smarlife.modellar.request.PostBookOrderModel
import com.example.smarlife.modellar.request.PostRegisterRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel : ViewModel() {


    // repasitoriyni elon qilish chaqirish
    val repository = ShopRepository()

    // Live Data Yaratish
    val error = MutableLiveData<String>()

    val token = MutableLiveData<String>()
    val status = MutableLiveData<Boolean>()

    // swipe qilgandan kegin progresni tuxtatish uchun
    val progress = MutableLiveData<Boolean>()

    // Mutable Live Data yaratish yani offer modelni ichida list kelib qolsa
    val offersData = MutableLiveData<List<OfferModel>>()

    val categorysData = MutableLiveData<List<CategoryModel>>()

    val top_product_Data = MutableLiveData<List<Top_Product_Model>>()

    val toifa_product_Data = MutableLiveData<List<CategoryModel>>()
    val user_Data = MutableLiveData<UserModel>()

    // bu repositoriylarga murojat qilish
    fun getOffers() {
        repository.getOffers(error, progress, offersData)
    }

    fun getCategories() {
        repository.getCategories(error, categorysData)
    }

    // user uchun
    fun getUser(){
        repository.getUser(error,user_Data)
    }

    fun getTopProduct() {
        repository.getTopProduct(error, top_product_Data)
    }

    fun getProductsByCategory(id: Int) {
        repository.getProductsByCategory(id, error, top_product_Data)
    }

    fun getToifa() {
        repository.getToifa(error, toifa_product_Data)
    }

    // token uchun
    fun gettoken(postRegisterRequest: PostRegisterRequest){
        repository.getToken(postRegisterRequest,token,error,status )
    }
    // book order uchun
    fun postBookOrder(getBokOrder: PostBookOrderModel){
        repository.getBookOrder(getBokOrder,error,status)
    }

    fun getProductsByIds(ids: String) {
        repository.getProductsByIds(ids, error, progress, top_product_Data)
    }

//    //DB uchun insert uchun TopProduct Uchun
//    fun insertAllProducts2DB(items: List<Top_Product_Model>) {
//        // (IO) BU Backgrount Patok Yani boshqa Patok
//        CoroutineScope(Dispatchers.IO).launch {
//
//            AppDataBase.getDatabase().getTopProductDao().deleteAll()
//            AppDataBase.getDatabase().getTopProductDao().insertAll(items)
//            CoroutineScope(Dispatchers.Main).launch {
//                error.value="Malumotlar Keshlandi"
//            }
//        }
//    }
//
//    fun getAllDBTopProducts() {
//        // (Main) BU Main Patok Yani Asosiy Patok
//        // bu backgraunt mainStreatdan malumot olib keladi
//        CoroutineScope(Dispatchers.Main).launch {
//            top_product_Data.value = withContext(Dispatchers.IO){
//                AppDataBase.getDatabase().getTopProductDao().getAllProduct()
//            }!!
//        }
//    }
//
//    //DB uchun insert uchun Categorys uchun
//    fun insertAllCategorys2DB(items: List<CategoryModel>) {
//        // (IO) BU Backgrount Patok Yani boshqa Patok
//        CoroutineScope(Dispatchers.IO).launch {
//
//            AppDataBase.getDatabase().getCategoryDao().deleteAll()
//            AppDataBase.getDatabase().getCategoryDao().insertAll(items)
////            CoroutineScope(Dispatchers.Main).launch {
////                error.value="Malumotlar Bazaga Yuklandi"
////            }
//        }
//    }
//
//    fun getAllDBCategorys() {
//        // (Main) BU Main Patok Yani Asosiy Patok
//        // bu backgraunt mainStreatdan malumot olib keladi
//        CoroutineScope(Dispatchers.Main).launch {
//            categorysData.value = withContext(Dispatchers.IO){
//                AppDataBase.getDatabase().getCategoryDao().getAllCategorys()
//            }!!
//        }
//    }
//
//





}

