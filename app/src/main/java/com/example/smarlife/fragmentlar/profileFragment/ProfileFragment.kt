package com.example.smarlife.fragmentlar.profileFragment

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.example.smarlife.Login.LoginOyna
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.changeLanguage.ChangeLanguageFragment
import com.example.smarlife.fragmentlar.favoritFragment.FavoriteFragment
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.utils.PrefUtils
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    val favoriteFragment = FavoriteFragment.newInstance()

    // bu viewModel bilan boglash
    lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        viewModel= ViewModelProvider(this).get(MainViewModel::class.java)
//
//        viewModel.top_product_Data.observe(this, Observer {
//
//            resayclerProductByIds.adapter= Top_Product_Adapter(it)
//
//        })
//        viewModel.progress.observe(this, Observer {
//            swipe.isRefreshing=it
//        })

    }





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)



    }


    companion object {
        @JvmStatic
        fun newInstance() = ProfileFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // bu intent qilish biror bir malumotni

//        val ffIsm = intent.getStringExtra("foydalanuvchiIsmi")
//        ism.setText()


        uzru.setOnClickListener {
            // bu BottomSheetDialogFragment() ni intent qilish usuli
            val fragment = ChangeLanguageFragment.newInstance()
            fragment.show(childFragmentManager, fragment.tag)
        }
        chiqish.setOnClickListener {

            var builder = AlertDialog.Builder(requireActivity())
            builder.setMessage("Profildan Chiqishni xohlaysizmi?")
            builder.setCancelable(true)
            builder.setNegativeButton(
                "Yo`q",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.cancel()
                })
            builder.setPositiveButton("Ha", DialogInterface.OnClickListener { dialogInterface, i ->

                requireActivity().finish()
                PrefUtils.logOut()
                var intent = Intent(requireActivity(), LoginOyna::class.java)
                startActivity(intent)


            })

            val alertDialog = builder.create()
            alertDialog.show()

        }

        sevimlilarRuyxati.setOnClickListener {
//            var intent=Intent(requireActivity(),FavoriteFragment::class.java)
//            startActivity(intent)
            var intent = Intent(requireActivity(), LoginOyna::class.java)
            startActivity(intent)

        }


    }
}