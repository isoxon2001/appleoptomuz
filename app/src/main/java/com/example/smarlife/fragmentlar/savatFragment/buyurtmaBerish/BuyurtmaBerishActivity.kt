package com.example.smarlife.fragmentlar.savatFragment.buyurtmaBerish

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.smarlife.MapsActivity
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.modellar.AdressModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.modellar.request.PostBookOrderModel
import com.example.smarlife.modellar.request.PostRegisterRequest
import com.example.smarlife.utils.Constants
import kotlinx.android.synthetic.main.activity_buyurtma_berish.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class BuyurtmaBerishActivity : AppCompatActivity() {
    // adressModelni Saqlash uchun elon qilinishi kerak
    var adress: AdressModel? = null
    lateinit var items: List<Top_Product_Model>

    // MainViewModel bilan boglash LiveData
    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyurtma_berish)

        // umumiy summani hisoblash uchun
        items = intent.getSerializableExtra(Constants.EXTRA_DATA) as List<Top_Product_Model>

        cardViewBackBuyurma.setOnClickListener {
            finish()
        }
        // eventBus ni eshitish Yani Event bus orqali junatilgan malumotni qabul qilish sharti
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        // umumiy Summani Xisoblash                                                  replace(" ","")===pustoy joyni yuq narsaga almashtirish
        tvTotalAmount.setText(items.sumByDouble {
            it.cardCount.toDouble() * (it.price_usd.replace(
                " ",
                ""
            ).toDoubleOrNull() ?: 0.0)
        }.toString())

        edAdress.setOnClickListener {
            startActivity(Intent(this, MapsActivity::class.java))
        }
        btnLocation.setOnClickListener {
            startActivity(Intent(this, MapsActivity::class.java))
        }

//        var userId: Int? = null
//        var allcount = 0
//        for (item in items) {
//            allcount += item.cardCount
//        }
//        var totalprice=items.sumByDouble {
//            it.cardCount.toDouble() * (it.price_usd.replace(
//                " ",
//                ""
//            ).toDoubleOrNull() ?: 0.0)
//        }.toString()
//        //var products=


        btnBuyurtmaBerish.setOnClickListener {

            // postbookorderuchun
//            val postBookOrder = PostBookOrderModel(
//                shopper_id = userId,
//                quantity = allcount,
//                total_price=totalprice,
//               // products=
//
////                edtName.text.toString().trim(),
////                phoneNumber!!,
////                binding.edtPssword.text.toString().trim(),
////                binding.edtAdres.text.toString().trim(),
//            )
            // post Zakazlarni
        }
       // viewModel.postBookOrder.postBookOrder

//        viewModel.getUser()
//        viewModel.user_Data.observe(this, androidx.lifecycle.Observer {
//            userId = it.id
//        })
    }


    // bu agar ruyhatdan utgan bulsa uni yangilash
    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    // bu metod evenBusni eshitish Yani Qbul qilib olish adressModel uzgaruvchisini
    @Subscribe
    fun onEven(adress: AdressModel) {
        this.adress = adress
        edAdress.setText("${adress.latitude},${adress.longitude}")
    }


}