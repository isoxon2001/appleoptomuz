package com.example.smarlife.fragmentlar.savatFragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.savatFragment.buyurtmaBerish.BuyurtmaBerishActivity
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.modellar.BookOrderModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.modellar.request.PostBookOrderModel
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import com.example.smarlife.view.SavatAdapter
import com.example.smarlife.view.SavatAdapterCallBack
import kotlinx.android.synthetic.main.activity_buyurtma_berish.*
import kotlinx.android.synthetic.main.fragment_savat.*
import java.io.Serializable

class SavatFragment : Fragment() {
    lateinit var viewModel: MainViewModel
    lateinit var items: List<Top_Product_Model>
    lateinit var bookordermodel: PostBookOrderModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.progress.observe(this, Observer {

            swipeSavat.isRefreshing = it
        })
        viewModel.error.observe(this, Observer {
            Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
        })

        // bu observe qiladi dlet ezilganda
        viewModel.top_product_Data.observe(this, Observer {

            // bu savat Adapterdan CallBack qilish
            resycleSavat.adapter = SavatAdapter(it, object : SavatAdapterCallBack {
                override fun onClickItem(item: Top_Product_Model) {
                    // bu count uchun
                    // PrefUtils.getCartCount(item.cardCount)
                    // bu delete qilish uchun
                    PrefUtils.deleteFromSavat(item.id)
                    loadData()
                }

            })
        })


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_savat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerSavat = view.findViewById(R.id.resycleSavat)
        resycleSavat.layoutManager = LinearLayoutManager(requireActivity())

        // swipe uchun
        swipeSavat.setOnRefreshListener {
            loadData()

        }

//        var userId: Int? = null
//        var allcount = 0
//        for (item in items) {
//            allcount += item.cardCount
//        }
//        var totalprice = items.sumByDouble {
//            it.cardCount.toDouble() * (it.price_usd.replace(
//                " ",
//                ""
//            ).toDoubleOrNull() ?: 0.0)
//        }.toString()
//
//        var product: ArrayList<BookOrderModel>? = null
//        for (it in SavatAdapter.list ?: emptyList()) {
//            product?.add(
//                BookOrderModel(
//                    it.id, it.cardCount, it.cardCount.toDouble() * (it.price_usd.replace(
//                        " ",
//                        ""
//                    ).toDoubleOrNull() ?: 0.0)
//                )
//            )
//        }
//
//
//        // post Zakazlarni
//
//
        btnBuyurtma.setOnClickListener {

            // zakaz berish


            // bu umumiy sumani buyurtma berish uchun olib utib beradi
            val intent = Intent(requireActivity(), BuyurtmaBerishActivity::class.java)

//            // postbookorderuchun
//            val postBookOrder = PostBookOrderModel(
//                shopper_id = userId,
//                quantity = allcount,
//                total_price = totalprice,
//                products = product!!
//            )

      //      intent.putExtra("AllTovar", "resycleSavat.text.toString().trim()")
            intent.putExtra(
                Constants.EXTRA_DATA,
                (viewModel.top_product_Data.value
                    ?: emptyList<Top_Product_Model>()) as Serializable
            )
            startActivity(intent)
          //  viewModel.postBookOrder(postBookOrder)
        }
//
//        // userid uchun
//        viewModel.getUser()
//        viewModel.user_Data.observe(requireActivity(), androidx.lifecycle.Observer {
//            userId = it.id
//        })
//
//
//        // call back ni observer qilish
//        viewModel.top_product_Data.observe(requireActivity(), Observer {
//            resycleSavat.adapter= SavatAdapter(it, object : SavatAdapterCallBack {
//                // bu categoris ezilganda mahsulotlar chiqishi uchun
//                override fun onClickItem(item: Top_Product_Model) {
//                    PrefUtils.deleteFromSavat(item.id).notifyDataSetChanged()
//
//                }
//            })
//        })


        loadData()


//        // swipe qilganda progress ni yangilaydi
//        viewModel.progress.observe(requireActivity(), Observer {
//            swipeSavat.isRefreshing=it
//        })


    }


    // bu Funksiya doim ochilganda yangilaydi
    override fun onResume() {
        super.onResume()
        loadData()


    }

    // bu map{it.product_id} api
    fun loadData() {
        var string = ""
        PrefUtils.getCardList().forEach {
            string = string + it.product_id.toString() + ","

        }
        viewModel.getProductsByIds(string.substringBeforeLast(','))
    }

    companion object {
        @JvmStatic
        fun newInstance() = SavatFragment()
        lateinit var recyclerSavat: RecyclerView
    }
}