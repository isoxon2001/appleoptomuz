package com.example.smarlife.api

import android.content.Context
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkManger {
    var retrofit: Retrofit? = null

    var api: Api? = null

    // token uchun shu yopiq ApiService ishlatiladi
    fun getApiService(): Api {
        if (api == null) {

            // api dan malumotni olib kelish
            retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                // RxJava yordamida Retrofit bilan Malumotlarni Patokda olib kelish uchun kerak
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()

            api = retrofit!!.create(Api::class.java)

        }
        return api!!
    }


    // Token olingandan kegin ishlatiladi
    fun getApiService1(): Api {
        val client = OkHttpClient.Builder()
//        if (BuildConfig.DEBUG) {
//            // bu intercepter uchun Cgucker uchun
//            okHttpClient.addInterceptor(ChuckerInterceptor.Builder(context).build())
//
//        }
            .addInterceptor(OAuthInterceptor("Bearer", PrefUtils.getToken())).build()
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .client(client)
            .build()
        api = retrofit?.create(Api::class.java)
        return api!!
    }
}