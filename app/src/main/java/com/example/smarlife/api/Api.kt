package com.example.smarlife.api

import com.example.smarlife.modellar.*
import com.example.smarlife.modellar.request.PostBookOrderModel
import com.example.smarlife.modellar.request.PostRegisterRequest
import io.reactivex.Observable
import retrofit2.http.*

interface Api {

    // appleoptomuz ni API si
    @GET("advertise")
    fun getOffers(): Observable<BaceResponse<List<OfferModel>>>

    @GET("category")
    fun getCategories(): Observable<BaceResponse<List<CategoryModel>>>

    @GET("product")
    fun getTopProduct(): Observable<BaceResponse<List<Top_Product_Model>>>

    @GET("product")
    fun getCategoryProducts(@Query("cat_id") categoryId: Int): Observable<BaceResponse<List<Top_Product_Model>>>

    // favorite ni post qilib junatib qabul qilish
    @GET("get_product_by_id")
    fun getProductById(@Query("products") request: String): Observable<BaceResponse<List<Top_Product_Model>>>

    //   registr ni post qilish
    @POST("register")
    fun postRegister(@Body request: PostRegisterRequest):Observable<BaceResponse<String>>

    // Users uchun
    @GET("user")
    fun getUser():Observable<BaceResponse<UserModel>>

    // orderlarni post qilish
    @POST("book-order")
    fun postBookOrder(@Body postBookOrderModel: PostBookOrderModel
    ):Observable<BaceResponse<String>>



//    @POST("register")
//    fun postRegister(@Query("phone,password,name,address") request: PostRegisterRequest):Observable<BaceResponse<String>>



    // OsonSavdo API si
//    @GET("get_offers")
//    fun getOffers(): Observable<BaceResponse<List<OfferModel>>>
//
//    @GET("get_categories")
//    fun getCategories(): Observable<BaceResponse<List<CategoryModel>>>
//
//
//    @GET("get_top_products")
//    fun getTopProduct(): Observable<BaceResponse<List<Top_Product_Model>>>
//
//
//    @GET("get_products/{cat_id}")
//    fun getCategoryProducts(@Path("cat_id")categoryId:Int): Observable<BaceResponse<List<Top_Product_Model>>>
//
//    @POST("get_products_by_ids")
//    fun getProductById(@Body request: GetProductsByIdsRequest): Observable<BaceResponse<List<Top_Product_Model>>>


}