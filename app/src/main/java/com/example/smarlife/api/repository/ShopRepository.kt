package com.example.smarlife.api.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.smarlife.api.NetworkManger
import com.example.smarlife.modellar.*
import com.example.smarlife.modellar.request.PostBookOrderModel
import com.example.smarlife.modellar.request.PostRegisterRequest
import com.example.smarlife.utils.PrefUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


class ShopRepository {

    // RxJava uchun
    val compositeDisposable = CompositeDisposable()

    fun getOffers(
       // context: Context,
        error: MutableLiveData<String>,
        progress: MutableLiveData<Boolean>,
        status: MutableLiveData<List<OfferModel>>
    ) {
        progress.value = true
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getOffers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<BaceResponse<List<OfferModel>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<OfferModel>>) {
                        progress.value = false
                        if (t.status) {
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        progress.value = false
                        error.value = e.localizedMessage
                    }
                })
        )

    }

    fun getCategories(
        error: MutableLiveData<String>,
        status: MutableLiveData<List<CategoryModel>>
    ) {

        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<BaceResponse<List<CategoryModel>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<CategoryModel>>) {
                        if (t.status) {
                            Log.d("abs", t.data.toString())
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        error.value = e.localizedMessage
                    }
                })
        )

    }


    // user uchun
    fun getUser(
        error: MutableLiveData<String>,
        status: MutableLiveData<UserModel>
    ) {
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableObserver<BaceResponse<UserModel>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<UserModel>) {
                        if (t.status) {
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        error.value = e.localizedMessage
                    }


                })
        )
    }


    fun getTopProduct(
        error: MutableLiveData<String>,
        status: MutableLiveData<List<Top_Product_Model>>
    ) {
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getTopProduct()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableObserver<BaceResponse<List<Top_Product_Model>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<Top_Product_Model>>) {
                        if (t.status) {
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        error.value = e.localizedMessage
                    }
                })
        )

    }

    fun getProductsByCategory(
        id: Int,
        error: MutableLiveData<String>,
        status: MutableLiveData<List<Top_Product_Model>>
    ) {
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getCategoryProducts(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableObserver<BaceResponse<List<Top_Product_Model>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<Top_Product_Model>>) {
                        if (t.status) {
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        error.value = e.localizedMessage
                    }
                })
        )

    }


    fun getProductsByIds(
        ids: String,
        error: MutableLiveData<String>,
        progress: MutableLiveData<Boolean>,
        status: MutableLiveData<List<Top_Product_Model>>
    ) {
        progress.value = true
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        val add = compositeDisposable.add(
            NetworkManger.getApiService1().getProductById(ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableObserver<BaceResponse<List<Top_Product_Model>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<Top_Product_Model>>) {
                        progress.value = false

                        if (t.status) {
                            t.data.forEach {
                                it.cardCount = PrefUtils.getCartCount(it)
                            }
                            status.value = t.data
                            if (t.status) {
                            } else {
                                error.value = t.message
                            }

                        }


                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        progress.value = false
                        error.value = e.localizedMessage
                    }


                })

        )
    }


    fun getToifa(
        error: MutableLiveData<String>,
        status: MutableLiveData<List<CategoryModel>>
    ) {
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService1().getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableObserver<BaceResponse<List<CategoryModel>>>() {
                    // Jarayon tugaganda OnComplete ni ichiga tushaqi
                    override fun onComplete() {
                    }

                    // Serverdan dan tugri Malumot Kelganda Shuni ichiga tushadi
                    override fun onNext(t: BaceResponse<List<CategoryModel>>) {
                        if (t.status) {
                            status.value = t.data
                        } else {
                            error.value = t.message
                        }
                    }

                    // Serverdan Notogri malumot kelganda shu joyga tushadi
                    override fun onError(e: Throwable) {
                        error.value = e.localizedMessage
                    }
                })
        )

    }

    /*
    *   "status" = true
    *   "data" =[{token:49|nQDKh5DxfXwsWsVXivyvwthJFqVExCjszT7V8mJn}]
    *   "message"="Successful"
    *
    * */
    fun getToken(
        postRegisterRequest: PostRegisterRequest,
        token: MutableLiveData<String>,
        error: MutableLiveData<String>,
        status: MutableLiveData<Boolean>
    ) {
        // api dan getOfferslarni RxJava Patok orqali olib kelish va MainView ga tashlaydi
        compositeDisposable.add(
            NetworkManger.getApiService().postRegister(postRegisterRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<BaceResponse<String>>() {
                    override fun onNext(t: BaceResponse<String>) {
                        status.value = t.status
                        token.value = t.data
                        error.value = t.message
                    }
                    override fun onError(e: Throwable) {
                        error.value = e.message
                    }
                    override fun onComplete() {
                    }
                })
        )
    }
    fun getBookOrder(
        getStringOrder: PostBookOrderModel,
        error: MutableLiveData<String>,
        status: MutableLiveData<Boolean>
    ){
        compositeDisposable.add(
            NetworkManger.getApiService1().postBookOrder(getStringOrder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<BaceResponse<String>>(){
                    override fun onNext(t: BaceResponse<String>) {
                        status.value=t.status
                        error.value=t.message
                    }

                    override fun onError(e: Throwable) {
                        error.value = e.message
                    }

                    override fun onComplete() {

                    }

                })
        )
    }

}