package com.example.smarlife
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.example.smarlife.db.AppDataBase
import com.example.smarlife.db.AppDataBase.Companion.initDatabase
import com.orhanobut.hawk.Hawk
import java.lang.Appendable

class App : MultiDexApplication(){

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)

        // Hawk ni ishlatish
        Hawk.init(this).build()

        // AppDataBase dan obekt oliboldik
        AppDataBase.initDatabase(this)
    }
}