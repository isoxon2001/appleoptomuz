package com.example.smarlife.Login.RuhatdanUtish

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smarlife.databinding.ActivityRegistrBinding
import com.example.smarlife.fragmentlar.main.MainActivity
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.fragmentlar.profileFragment.ProfileFragment
import com.example.smarlife.modellar.request.PostRegisterRequest
import com.example.smarlife.utils.PrefUtils
import kotlinx.android.synthetic.main.activity_registr.*

class RegistrActivity : AppCompatActivity() {
    lateinit var binding: ActivityRegistrBinding
    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistrBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bu intent qilish biror bir malumotni
        val phoneNumber = intent.getStringExtra("phoneNumber")
        binding.edtPhone.setText(phoneNumber)
        btnRuyxatdanUtish.setOnClickListener {

            // postregistr
            val postRegisterRequest = PostRegisterRequest(
                binding.edtName.text.toString().trim(),
                phoneNumber!!,
                binding.edtPssword.text.toString().trim(),
                binding.edtAdres.text.toString().trim()
            )

//            val intent1=Intent(this,ProfileFragment::class.java)
//            // bu isimni intent qiish
//            intent1.putExtra("foydalanuvchiIsmi",binding.edtName.toString())


            viewModel = MainViewModel()
            viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
            viewModel.gettoken(postRegisterRequest)
            // bu Observer qilish
            viewModel.token.observe(this, Observer { token ->
                if (token.isNotEmpty()) {

                    PrefUtils.setToken(token)
                    PrefUtils.setFoydalanuvchi(binding.edtName.text.toString().trim())

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            })
            viewModel.error.observe(this, { e ->
                Toast.makeText(this, "$e", Toast.LENGTH_SHORT).show()

            })


        }

    }

    fun loadData() {


    }

}