package com.example.smarlife.Login

//import io.grpc.util.AdvancedTlsX509TrustManager
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smarlife.Login.RuhatdanUtish.RegistrActivity
import com.example.smarlife.databinding.ActivityLoginBinding
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.utils.PrefUtils
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit


class LoginOyna : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var binding: ActivityLoginBinding
    private val TAG = "LoginOyna"
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        FirebaseApp.initializeApp(this)


        binding.kirish1.setOnClickListener {
            startPhoneNumberVerification(binding.tel.text.toString())

           // binding.tel.isFocusable = true

            binding.LLsms.visibility = View.VISIBLE
            binding.kirish1.visibility = View.GONE
            binding.kirish2.visibility = View.VISIBLE
            binding.qaytaSms.visibility = View.VISIBLE
        }
        binding.kirish2.setOnClickListener {
            verifyPhoneNumberWithCode(storedVerificationId, binding.edtSMS.text.toString())

        }

        auth = Firebase.auth

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                Log.d("key", "onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.w("key2", "onVerificationFailed", e)

                if (e is FirebaseAuthInvalidCredentialsException) {

                } else if (e is FirebaseTooManyRequestsException) {

                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                super.onCodeSent(verificationId, token)
                Log.d(TAG, "onCodeSent : $verificationId")

                storedVerificationId = verificationId
                resendToken = token
            }
        }
    }


    // override fun ... bu Abstrak funksiya bu funksiyani boshqa klasslarda chaqirib olish mumkin
    //   private fun ... bu Bu funksiyani boshqa klass larda chaqirib olib bulmaydi

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun startPhoneNumberVerification(phonenumber: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+998$phonenumber")  // phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // time out and unit
            .setActivity(this)  // activity for (callback binding)
            .setCallbacks(callbacks) // onVerificationStateChanchedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        // [END start_phone_auth]
    }

    private fun verifyPhoneNumberWithCode(verificationId: String?, code: String) {
        val credential = PhoneAuthProvider.getCredential(verificationId!!, code)
        signInWithPhoneAuthCredential(credential)

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithCredential:success")
                    val user = task.result?.user
                    Toast.makeText(
                        this,
                        "AppleOptomUz ga Hush Kelibsiz :" + user,
                        Toast.LENGTH_SHORT
                    ).show()


                    // SmS kelgandan kegin kegin Register ni ochish
                    var intent = Intent(this, RegistrActivity::class.java)
                    // bu put extra qilish biror bir malumotni
                    intent.putExtra("phoneNumber", "+998${binding.tel.text.toString().trim()}")
                    startActivity(intent)
                    finish()


                    viewModel = MainViewModel()
                    viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
                    // bu Observer qilish
                    viewModel.token.observe(this, Observer { token ->
//                        if (token.isNotEmpty()) {
//
//                            PrefUtils.setToken(token)
////                            PrefUtils.setFoydalanuvchi(token[0].id)
//
//
//                            // SmS kelgandan kegin kegin Register ni ochish
//                            var intent = Intent(this, RegistrActivity::class.java)
//                            intent.putExtra(
//                                "phoneNumber",
//                                "+998${binding.tel.text.toString().trim()}"
//                            )
//                            startActivity(intent)
//                            finish()
////
//                        }
                    })
                    viewModel.error.observe(this, { e ->
                        Toast.makeText(this, "$e", Toast.LENGTH_SHORT).show()

                    })


                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(this, "AppleOptomUz :" + task.exception, Toast.LENGTH_SHORT)
                            .show()

                    }
                }
            }

    }

    private fun updateUI(user: FirebaseUser? = auth.currentUser) {
//        startActivity(Intent(this,MainActivity::class.java))


    }

    companion object {
        private const val TAG = "MainActivity"
    }


}

