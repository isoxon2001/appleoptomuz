package com.example.smarlife.modellar

data class UserModel(
    val id:Int,
    val name:String,
    val role_id:Int,
    val phone:String,
    val address:String
)