package com.example.smarlife.modellar.request

data class GetProductsByIdsRequest (
    val products:List<String>
        )