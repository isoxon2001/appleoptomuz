package com.example.smarlife.modellar

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


// Bu Room uchun
@Entity(tableName = "product")
data class Top_Product_Model(
    // bu id larni takrorlanmasligi uchun
    @PrimaryKey(autoGenerate = true)
    val uid: Int,
    val id: Int,
    var name_uz: String,
    val price_usd: String,

    val image: String,
    var cat_id:Int,
    var description_uz:String,
    var cardCount: Int

    // osonsavdo
//    var name: String,
//    val price: String,

) : Serializable