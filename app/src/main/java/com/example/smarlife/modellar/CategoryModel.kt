package com.example.smarlife.modellar

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
// Bu Room uchun
@Entity(tableName = "category")
data class CategoryModel(
    @PrimaryKey(autoGenerate = true)
    val uid:Long=0,
    var id:Int,
    var name_uz:String,
    var name_ru:String,
    var image:String,

    // oson savdo niki
//    var title:String,
//    var icon:String,
    var checked:Boolean=false
):Serializable