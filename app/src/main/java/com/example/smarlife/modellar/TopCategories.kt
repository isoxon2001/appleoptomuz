package com.example.smarlife.modellar

data class TopCategories(
    val id:Int,
    val name:String,
    val price:String,
    val image:String,
    val cat_id:Int
)