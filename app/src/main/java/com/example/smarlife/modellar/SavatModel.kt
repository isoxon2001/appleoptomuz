package com.example.smarlife.modellar

import java.io.Serializable

data class SavatModel(
    val product_id:Int,
    var count:Int
):Serializable