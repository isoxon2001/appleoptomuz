package com.example.smarlife.modellar.request

data class PostRegisterRequest(
    val name: String,
    val phone: String,
    val password: String,
    val address: String,
    )