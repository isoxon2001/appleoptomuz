package com.example.smarlife.modellar.request

import com.example.smarlife.modellar.BookOrderModel
import java.io.Serializable

data class PostBookOrderModel(
    val shopper_id:Int?,
    val quantity:Int,
    val total_price:String,
    val products:ArrayList<BookOrderModel>
):Serializable