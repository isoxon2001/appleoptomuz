package com.example.smarlife.modellar

import java.io.Serializable

data class AdressModel(
    val adress: String,
    val latitude:Double,
    val longitude:Double

):Serializable