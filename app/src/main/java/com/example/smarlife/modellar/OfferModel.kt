package com.example.smarlife.modellar

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


// Bu Room uchun
@Entity(tableName = "advertise")
data class OfferModel(
    @PrimaryKey(autoGenerate = true)
    val uid:Long=0,
    val id: Int,
    val image:String,
    val created_at:String,
    val updated_at:String,
    ):Serializable