package com.example.smarlife.view

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.productdetail.ProductDetailActivity
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import kotlinx.android.synthetic.main.toifa_ochish.view.*

class AdapterToifaOchish(val items: List<Top_Product_Model>) : RecyclerView.Adapter<AdapterToifaOchish.ItemHolder>() {

    class ItemHolder(view: View) : RecyclerView.ViewHolder(view)

    // Bu Layoutga Ulab Beruvchi Funksiya

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterToifaOchish.ItemHolder {
        return AdapterToifaOchish.ItemHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.toifa_ochish, parent, false)
        )



    }

    override fun onBindViewHolder(holder: AdapterToifaOchish.ItemHolder, position: Int) {

        holder.itemView.setOnClickListener {
            val intent= Intent(it.context, ProductDetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_DATA,items[position])
            it.context.startActivity(intent)


        }







        holder.itemView.toifa_nomi.text=items[position].name_uz

        holder.itemView.toifa_narx.text=items[position].price_usd



        Glide.with(holder.itemView).load(Constants.HOST_IMAGE+items[position].image).into(holder.itemView.imgToifa)
        Log.d("azxc",items.toString())



//        holder.itemView.tvSavatNarx.text=item.price
//        holder.itemView.tvSavatNomi.text=item.name
//
//        Glide.with(holder.itemView).load(Constants.HOST_IMAGE+item.image).into(holder.itemView.savatImg)
//
//        holder.itemView.tvCount.text=item.cardCount.toString()

    }

    override fun getItemCount(): Int {
        return items.count()

    }

}