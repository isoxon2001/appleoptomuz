package com.example.smarlife.view

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.ToifaOchishActivity
import com.example.smarlife.modellar.CategoryModel
import com.example.smarlife.utils.Constants
import kotlinx.android.synthetic.main.toifalar_item_layout.view.*


class ToifalarAdapter(val items: List<CategoryModel>) : RecyclerView.Adapter<ToifalarAdapter.ItemHolder>(){
    class ItemHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToifalarAdapter.ItemHolder {
        return ToifalarAdapter.ItemHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.toifalar_item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ToifalarAdapter.ItemHolder, position: Int) {
        val item = items[position]

        // catigorislar ezilganini kursatish
        // item larni va icon title ni junatish uchun yoziladi Extra Data putEXTRA qilinadi
        holder.itemView.setOnClickListener {
           val intent=Intent(holder.itemView.context,ToifaOchishActivity::class.java)
            intent.putExtra(Constants.EXTRA_DATA,item.id)
            intent.putExtra("title",item.name_uz)
            intent.putExtra("icon",Constants.HOST_IMAGE+item.image)
            holder.itemView.context.startActivity(intent)

        }
        Log.e("bbb",item.toString())


        holder.itemView.tvToifaTitle.text = item.name_uz // layoutdagi textView ga id orqali titldi joylash

        Glide.with(holder.itemView.context).load(Constants.HOST_IMAGE+item.image).into(holder.itemView.imgIcon)

        // Animatsiya Uchun CardViewga
        holder.itemView.ToifaCardView.startAnimation(AnimationUtils.loadAnimation(holder.itemView.context,R.anim.animation_for_card))

//        if (item.checked){
//            holder.itemView.ToifaCardView.setCardBackgroundColor(ContextCompat.getColor(holder.itemView.context,R.color.colorPrimery))
//            holder.itemView.tvToifaTitle.setTextColor(Color.WHITE)
//        }else{
//            holder.itemView.ToifaCardView.setCardBackgroundColor(ContextCompat.getColor(holder.itemView.context,R.color.gray))
//            holder.itemView.tvToifaTitle.setTextColor(Color.BLACK)
//
//        }


    }

    override fun getItemCount(): Int {
        return items.count()
    }

}