package com.example.smarlife.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.main.MainViewModel
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import com.example.smarlife.utils.PrefUtils
import kotlinx.android.synthetic.main.savat_item_layout.view.*

// bu Interface Categoriya ezilganda Mahsulot chiqishi uchun
interface SavatAdapterCallBack {
    fun onClickItem(item: Top_Product_Model)
}

class SavatAdapter(var items: List<Top_Product_Model>, val callBack: SavatAdapterCallBack) :
    RecyclerView.Adapter<SavatAdapter.ItemHolder>() {
    class ItemHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        var list: List<Top_Product_Model>? = null
    }


    lateinit var viewModel: MainViewModel


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        list = items
        return ItemHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.savat_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.count()

    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = items[position]


        // plus ezilganda qushadi
        holder.itemView.imgPlus.setOnClickListener {
            item.cardCount += 1
            holder.itemView.tvCount.text = item.cardCount.toString()
            PrefUtils.setCard(item)
        }

        // minus ezilganda ayiradi
        holder.itemView.imgMinus.setOnClickListener {
            if (item.cardCount == 1) {
                PrefUtils.deleteFromSavat(item.id)
                items = items.filter { it.id != item.id }
                notifyDataSetChanged()
            } else {
                item.cardCount -= 1
                PrefUtils.setCard(item)
                // bu itemni yangilaydi
                notifyItemChanged(position)
            }
        }

        // bu funksiya delet qilgani kerak
        holder.itemView.imgDelete.setOnClickListener {
            //callBack.onClickItem(item)
            if (item.cardCount == 1) {
                PrefUtils.deleteFromSavat(item.id)
                items = items.filter { it.id != item.id }
                notifyDataSetChanged()
            } else {
                item.cardCount -= 1
                PrefUtils.setCard(item)
                // bu itemni yangilaydi
                notifyItemChanged(position)
            }
        }
        holder.itemView.tvSavatNarx.text = item.price_usd
        holder.itemView.tvSavatNomi.text = item.name_uz

        Glide.with(holder.itemView).load(Constants.HOST_IMAGE + item.image)
            .into(holder.itemView.savatImg)

        holder.itemView.tvCount.text = item.cardCount.toString()
    }


}