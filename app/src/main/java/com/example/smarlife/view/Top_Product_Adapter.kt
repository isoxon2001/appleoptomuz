package com.example.smarlife.view

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.productdetail.ProductDetailActivity
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants
import kotlinx.android.synthetic.main.top_product_item.view.*

class Top_Product_Adapter(val items: List<Top_Product_Model>):RecyclerView.Adapter<Top_Product_Adapter.ItemHolder>(){
    class ItemHolder(view: View) : RecyclerView.ViewHolder(view)

//    lateinit var categoryModel: CategoryModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.top_product_item,parent,false)
        )
    }

    override fun getItemCount(): Int {
    return items.count()
    }

    override fun onBindViewHolder( holder: ItemHolder, position: Int) {
      val item=  items[position]
        holder.itemView.setOnClickListener {
            val intent=Intent(it.context,ProductDetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_DATA,item)
            it.context.startActivity(intent)
        }
        // Animatsiya Uchun CardViewga
        holder.itemView.top_productCard.startAnimation(AnimationUtils.loadAnimation(holder.itemView.context,R.anim.animation_for_card))

        // Bu Imageni Apidan olib kelish va qayerga olib kelishni kursatish
        Glide.with(holder.itemView.context).load(Constants.HOST_IMAGE+item.image).into(holder.itemView.top_productImg)
        // Log.e Debug Orqali Apini (polyani) malumotlarini tekshiradi
        Log.e("eee", item.toString())
        // bu ismini qayerga olib kelihni kursatish
        holder.itemView.tvTopProductNomi.text=item.name_uz
        // bu narxini qayerga olib kelishni kursatish
        holder.itemView.tvTopProductNarx.text=item.price_usd
        // bu categoriyasini nomini olib keladi
//        if (item.cat_id== (this.categoryModel.id).toString()){
//
//            holder.itemView.tvTopProductCategoryNomi.text=this.categoryModel.title
//        }



    }

}
