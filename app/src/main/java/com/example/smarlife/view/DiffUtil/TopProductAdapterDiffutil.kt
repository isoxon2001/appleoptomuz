package com.example.smarlife.view.DiffUtil

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smarlife.R
import com.example.smarlife.databinding.TopProductItemBinding
import com.example.smarlife.fragmentlar.productdetail.ProductDetailActivity
import com.example.smarlife.modellar.Top_Product_Model
import com.example.smarlife.utils.Constants

class TopProductAdapterDiffutil: ListAdapter<Top_Product_Model,
        TopProductAdapterDiffutil.TopProductViewHolder>(DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopProductViewHolder {
        val binding =
            TopProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return TopProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TopProductViewHolder, position: Int) {
        holder.bind(getItem(position), holder.itemView.context)
    }


    class TopProductViewHolder(private val binding: TopProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Top_Product_Model, context: Context) {
            binding.topProductCard.setOnClickListener {
                val intent = Intent(context,ProductDetailActivity::class.java)
                intent.putExtra(Constants.EXTRA_DATA, item)
                it.context.startActivity(intent)
            }

            // Animatsiya Uchun CardViewga
            binding.topProductCard.startAnimation(
                AnimationUtils.loadAnimation(
                    context,
                    R.anim.animation_for_card
                )
            )

            // Bu Imageni Apidan olib kelish va qayerga olib kelishni kursatish
            Glide.with(context).load(Constants.HOST_IMAGE + item.image)
                .into(binding.topProductImg)
            // Log.e Debug Orqali Apini (polyani) malumotlarini tekshiradi
            Log.e("eee", item.toString())
            // bu ismini qayerga olib kelihni kursatish
            binding.tvTopProductNomi.text = item.name_uz
            // bu narxini qayerga olib kelishni kursatish
            binding.tvTopProductNarx.text = item.price_usd
            // bu categoriyasini nomini olib keladi





        }

    }

    class DiffCallback : DiffUtil.ItemCallback<Top_Product_Model>() {
        override fun areItemsTheSame(
            oldItem: Top_Product_Model,
            newItem: Top_Product_Model
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Top_Product_Model,
            newItem: Top_Product_Model
        ): Boolean {
            return oldItem == newItem
        }

    }


}
