package com.example.smarlife.view

import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.smarlife.R
import com.example.smarlife.fragmentlar.ToifaOchishActivity
import com.example.smarlife.modellar.CategoryModel
import com.example.smarlife.utils.Constants
import kotlinx.android.synthetic.main.category_item_layout.view.*

// bu Interface Categoriya ezilganda Mahsulot chiqishi uchun
interface CategoryAdapterCallBack{
   fun onClickItem(item:CategoryModel)
}
class CategoryAdapter(val items: List<CategoryModel>, val callBack: CategoryAdapterCallBack) : RecyclerView.Adapter<CategoryAdapter.ItemHolder>() {

    class ItemHolder(view: View) : RecyclerView.ViewHolder(view)


    // Bu Layoutga Ulab Beruvchi Funksiya
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.category_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = items[position]

        // catigorislar ezilganini kursatish
        holder.itemView.setOnClickListener {
          items.forEach{
              it.checked =false
          }
            item.checked =true
            // category ezilganda chiqarish
//            callBack.onClickItem(item)
//            notifyDataSetChanged()
            // bu ezilganda ochilishi uchun
            val intent= Intent(holder.itemView.context, ToifaOchishActivity::class.java)
            intent.putExtra(Constants.EXTRA_DATA,item.id)
            intent.putExtra("title",item.name_uz)
            intent.putExtra("icon", Constants.HOST_IMAGE+item.image)
            holder.itemView.context.startActivity(intent)
        }
        Log.e("bbb",item.toString())


        holder.itemView.categorytvTitle.text = item.name_uz // layoutdagi textView ga id orqali titldi joylash

        // Animatsiya Uchun CardViewga
        holder.itemView.categoryCardView.startAnimation(AnimationUtils.loadAnimation(holder.itemView.context,R.anim.animation_for_card))

        if (item.checked){
            holder.itemView.categoryCardView.setCardBackgroundColor(ContextCompat.getColor(holder.itemView.context,
                R.color.purple_550
            ))
            holder.itemView.categorytvTitle.setTextColor(Color.WHITE)
        }else{
            holder.itemView.categoryCardView.setCardBackgroundColor(ContextCompat.getColor(holder.itemView.context,R.color.gray))
            holder.itemView.categorytvTitle.setTextColor(Color.BLACK)

        }


//        // bu kategorya nomini kursatadi
//        holder.itemView.tvTopProductCategoryNomi.text=item.title




    }
}