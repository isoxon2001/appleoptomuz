package com.example.smarlife.utils

import com.example.smarlife.modellar.SavatModel
import com.example.smarlife.modellar.Top_Product_Model
import com.orhanobut.hawk.Hawk
import java.util.ArrayList

object PrefUtils {
    // Bu Xotira(KESH)ga "pref_favarites" deb saqlaydi
    const val PREF_FAVORITES = "pref_favorites"
    const val PREF_SAVAT = "get_savat"
    const val PREF_TOKEN = "token"
    const val PREF_FCM_TOKEN = "pref_fcm_token"
    const val PREF_ID = "id"
    const val PREF_USER = "ismi"

    // bu qaysi Model dagi  item larni saqlashni kursatadi
    // bu Tanlanga item larni set qiladi
    fun setFavorite(item: Top_Product_Model) {
        val items = Hawk.get(PREF_FAVORITES, arrayListOf<Int>())
        // bu yerda shart berilyapdi
        if (items.filter { it == item.id }.firstOrNull() != null) {
            items.remove(item.id)
        } else {
            items.add(item.id)
        }
        Hawk.put(PREF_FAVORITES, items)
    }

    // bu funksiya tanlanganlarni get qiladi
    fun getFavoriteList(): ArrayList<Int> {

        return Hawk.get(PREF_FAVORITES, arrayListOf<Int>())
    }

    // bu funksiya Favorit ruyxatda bormi yoki yuq tekshiradi
    fun checkFavorite(item: Top_Product_Model): Boolean {
        val items = Hawk.get(PREF_FAVORITES, arrayListOf<Int>())
        return items.filter { it == item.id }.firstOrNull() != null


    }

    // Savatga Biriktirish
    fun setCard(item: Top_Product_Model) {
        val items = Hawk.get<ArrayList<SavatModel>>(PREF_SAVAT, arrayListOf<SavatModel>())
        val card = items.filter { it.product_id == item.id }.firstOrNull()
        if (card != null) {
            if (item.cardCount > 0) {
                card.count = item.cardCount
            } else {
                items.remove(card)
            }
        } else {
            val newCard = SavatModel(item.id, item.cardCount)
            items.add(newCard)
        }
        Hawk.put(PREF_SAVAT, items)
    }

    fun deleteFromSavat(id: Int) {
        val items = Hawk.get<ArrayList<SavatModel>>(PREF_SAVAT, arrayListOf<SavatModel>())
        if (items.size > 0) {
            var deleteItem: SavatModel? = null
            for (item in items) {
                if (item.product_id == id) {
                    deleteItem = item
                }
            }
            items.remove(deleteItem)
            Hawk.put(PREF_SAVAT, items)
        }
    }

    fun getCardList(): ArrayList<SavatModel> {
        return Hawk.get(PREF_SAVAT, arrayListOf<SavatModel>())
    }

    fun getCartCount(item: Top_Product_Model): Int {
        val items = Hawk.get<ArrayList<SavatModel>>(PREF_SAVAT, arrayListOf<SavatModel>())
        return items.filter { it.product_id == item.id }.firstOrNull()?.count ?: 0
    }

    // token ni set qilish
    // va doim kirganda hawkga saqlab ketish

    fun setToken(token: String) {
        var tokenn = Hawk.get(PREF_TOKEN, String())
        if (!tokenn.isEmpty()) {
            Hawk.delete(PREF_TOKEN)
            Hawk.put(PREF_TOKEN, token)
        } else {
            Hawk.put(PREF_TOKEN, token)
        }
    }

    fun setFoydalanuvchi(ismi: String) {
        var tokenn = Hawk.get(PREF_USER, String())
        Hawk.delete(PREF_USER)
        Hawk.put(PREF_USER, ismi)

    }

    fun getFoydalanuvchi(): String {
        return Hawk.get(PREF_USER)
    }


    // splash uchun
    fun getToken(): String {
        return Hawk.get(PREF_TOKEN, String())
    }

    // FCM uchun
    fun setFCMToken(value: String) {
        Hawk.put(PREF_FCM_TOKEN, value)

    }

    fun getFCMToken(): String {
        return Hawk.get(PREF_FCM_TOKEN, "")
    }

    //Login Uchun
    fun logOut() {
        Hawk.delete(PREF_ID)
        Hawk.delete(PREF_TOKEN)
    }
}